//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const url = require('url');

const logger = require('../../logger');
const Instance = require('../instance');
const AbstractDiscoveryService = require('../abstract.discovery.service');

class WebDiscoveryService extends AbstractDiscoveryService {

  /**
   * Discover all the reachable instances of the requested service.
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support
   * @returns {Array<Service>} All discovered instances
   * 
   * @public
   * @implements AbstractDiscoveryService#discover(service)
   */
  doDiscover(backend) {
    let answer = [];
    let _url = url.parse(backend.target);
    logger.debug('parsed URL', JSON.stringify(_url));
    answer.push(new Instance('1', _url.protocol, _url.hostname, _url.port, _url.pathname));
    return Promise.resolve(answer);
  }

  /**
   * Validate the configuration provided is as expected for the requested backend.
   * Specifically validate that **target** parameter is provided.
   * 
   * @param {Any} backend the backend to discover, definition varies 
   * depending on the backend to support 
   * @throws {Error} if the validation is not successful
   * 
   * @protected
   */
  validate(backend) {
    if (!backend.target) {
      throw new Error('target parameter must be provided for web backend definition');
    }
  }
}

/**
 * A noop discovery service that allow to create services for web-base proxy.
 * 
 * @public
 * @implements AbstractDiscoveryService
 * @typedef DockerDiscoveryService
 */
module.exports = WebDiscoveryService;