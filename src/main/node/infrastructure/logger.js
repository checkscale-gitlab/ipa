/* global module */
/* global require */
/* global process */

'use strict';

const winston = require('winston');

const level = process.env.LOG_LEVEL || 'info';

// const formatter = function (info) {
//   return `${info.timestamp()} - ${info.level} [places,${httpContext.get('traceId')},${httpContext.get('spanId')},false]: ${info.message}`;
// };

const logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: level,
      timestamp: function () {
        return (new Date()).toISOString();
      }
    })
  ]
});

logger.isDebug = function () {
  return this.level == 'debug';
};

module.exports = logger;