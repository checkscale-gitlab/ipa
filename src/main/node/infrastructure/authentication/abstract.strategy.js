//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const Component = require('dino-core').Component;

const logger = require('../logger');

class AbstractStrategy extends Component {

  constructor({ applicationContext, environment }) {
    super();

    this.applicationContext = applicationContext;
    this.environment = environment;
  }

  /**
   * Allow access the application context from implementations of this abstract class.
   * 
   * @protected
   */
  getApplicationContext() {
    return this.applicationContext;
  }

  getEnvironment() {
    return this.environment;
  }

  getStrategyConfiguration(strategy) {
    logger.debug(`configuration for ${strategy}`, JSON.stringify(this.environment.get('authentication').strategies[strategy]));
    return this.environment.get('authentication').strategies[strategy].configuration;
  }

  /**
   * Activate and configure the current strategy
   * @param {Any} configuration the authentication API configuration
   * 
   * @public
   * @abstract
   */
  activate(configuration) {
    this.configuration = configuration;
    this.doActivate(configuration.authority.name || 'noopAuthority');
  }

  generateStrategyName(prefix) {
    return `${prefix}_${this.configuration.api}`;
  }

  /**
   * Allow to retrieve the authenticator for the current strategy.
   * 
   * @returns {Function} the authenticator
   * @public
   * @abstract
   */
  authenticator() { }

  /**
   * Execute the strategy-specific logic to configure and perform authentication and if requested authorization.
   */
  doActivate(authority) { // eslint-disable-line no-unused-vars
    throw new Error('not implemented');
  }

  /**
   * Validate the authenticated identity
   * 
   * @param {String} authority the identity validator to use
   * @param {Profile} profile the profile retrieved during authentication
   * @param {Function} done a callback invoked to signal the identity validation outcome
   * 
   * @protected
   */
  authorize(authority, profile, done) {
    logger.debug(`authorize ${profile.getId()} with ${authority}`);
    return this.getApplicationContext().resolve(authority)
      .validate(profile, this.configuration)
      .then(answer => done(null, answer))
      .catch(error => done(error, null));
  }
}

/**
 * @public
 * @abstract
 * @typedef AbstractStrategy
 */
module.exports = AbstractStrategy;