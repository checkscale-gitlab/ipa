//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */

class Profile {

  /**
   * Create a new profile instance.
   * @param {String} id the profile identifier shared between the authentication 
   * provider and the systems behind this API Gateway, should always be used to validate the identity.
   * @param {String} provider the authentication provider
   * 
   * @private
   */
  constructor(id, provider) {
    this.id = id;
    this.provider = provider;
  }

  getId() {
    return this.id;
  }

  getProvider() {
    return this.provider;
  }

  /**
   * A factory method to create Profile instances
   * 
   * @param {String} id the profile identifier shared between the authentication 
   * provider and the systems behind this API Gateway, should always be used to validate the identity.
   * @param {String} provider the authentication provider
   * 
   * @public
   * @static
   */
  static create(id, provider) {
    return new Profile(id, provider);
  }
}

/**
 * A standard user profile to be shared across authentication strategy and identity validators
 * @typedef Profile
 * @public
 */
module.exports = Profile;