//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

'use strict';

const logger = require('../logger');
const AbstractPolicy = require('./abstract.policy');

class SpikeArrestPolicy extends AbstractPolicy {

  constructor() {
    super();
    this.keys = {};
  }

  /**
   * Apply the Spike Arrest policy
   * @param {Any} req the reuest
   * 
   * @protected
   * @implements AbstractPolicy#apply(req)
   * @see https://github.com/apigee-127/volos/blob/master/spikearrest/memory/lib/memory_spikearrest.js
   */
  apply(req, res) { // eslint-disable-line no-unused-vars
    let now = Date.now();
    let key = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
    let weight = 1;
    let windowSize = (+this.configuration.interval / 1000) / +this.configuration.allow;

    let allowed = true;
    let bucket = this.keys[key];
    if (!bucket) { bucket = {}; this.keys[key] = bucket; }

    logger.debug('Bucket', bucket);

    if (now < bucket.windowExpires) {
      bucket.used = bucket.used + weight || weight;
      allowed = false;
    } else {
      this.keys[key] = bucket;
      bucket.windowExpires = now + (weight * windowSize);
      bucket.used = weight;
    }

    return allowed;
  }

  /**
   * Allow to configure this policy. Accepted configuration parameters are:
   *  - **interval**, the interval at which the spike arrest shold be reset expressed in ms
   *  - **allow**, the number of requests that are allowed inside the window interval
   * @param {Any} configuration the policy configuration
   * 
   * @public
   */
  configure(configuration) {
    this.configuration = configuration;
  }

  /**
   * Execute logic that will signal that the policy has denied the client request
   * @param {Any} res the client response
   * 
   * @protected
   * @implements AbstractPolicy#onDeny(res)
   */
  onDeny(res) {
    res.status(429).json({ message: 'Too Many Requests' });
  }

  static scope() {
    return 'transient';
  }
}

/**
 * @typedef SpikeArrestPolicy
 * @public
 */
module.exports = SpikeArrestPolicy;