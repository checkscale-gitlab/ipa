//    Copyright 2018 Quirino Brizi [quirino.brizi@gmail.com]
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

/* global module */
/* global require */

const Component = require('dino-core').Component;

class PolicyFactory extends Component {

  constructor({ applicationContext }) {
    super();

    this.applicationContext = applicationContext;
  }

  /**
   * Get a policy object
   * @param {String} policy the policy to loookup
   * @returns {AbstractPolicy} an implementation of AbstractPolicy
   * 
   * @public
   */
  getPolicy(policy) {
    return this.applicationContext.resolve(policy);
  }
}

/**
 * @typedef PolicyFactory
 */
module.exports = PolicyFactory;