FROM node:10.9.0-alpine

LABEL maintainer="Quirino Brizi <quirino.brizi@gmail.com>"

RUN mkdir -p /app

WORKDIR /app

ADD package.json index.js /app/
ADD ./src/main /app/src/main

RUN npm set progress=false && npm config set depth 0 && npm cache clean --force
RUN npm install --production

EXPOSE 3030

CMD ["npm", "start"]
